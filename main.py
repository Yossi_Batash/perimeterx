from __future__ import absolute_import

import argparse
import logging
import re

import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.io import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions


def run(argv=None):
    """Main entry point; defines and runs the wordcount pipeline."""

    parser = argparse.ArgumentParser()
    parser.add_argument('--input',
                        dest='input',
                        default='/Users/yossibatash/Dev_Env/perimeterx/input/input.txt',#'gs://perimeterx-input/kinglear.txt',
                        help='Input file to process.')
    parser.add_argument('--output',
                        dest='output',
                        default='/Users/yossibatash/Dev_Env/perimeterx/output/output.txt',
                        help='Output file to write results to.')
    known_args, pipeline_args = parser.parse_known_args(argv)
    pipeline_args.extend([
        # run your pipeline on the Google Cloud Dataflow Service.
        '--runner=DirectRunner',
        # project ID is required in order to run the pipeline on Google Cloud Dataflow Service.
        '--project=perimetrx',
        # Google Cloud Storage/local path is required for staging local files.
        '--staging_location=/Users/yossibatash/Dev_Env/perimeterx/staging',
    ])

    # We use the save_main_session option because one or more DoFn's in this
    # workflow rely on global context (e.g., a module imported at module level).
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True


    with beam.Pipeline(options=pipeline_options) as p:

        # The DoFn to perform on each element in the input PCollection.
        class SplitToWords(beam.DoFn):
            def process(self, element):
                return re.findall(r'[A-Za-z\']+', element)

        class FormatAsText(beam.DoFn):
            def process(self, element):
                word, count = element
                yield '%s: %s' % (word, count)

        # Read the text file[pattern] into a PCollection.
        lines = p | ReadFromText(known_args.input)

        # Split the text into words
        words = lines | beam.ParDo(SplitToWords()) #'Split' >> (beam.FlatMap(lambda x: re.findall(r'[A-Za-z\']+', x)).with_output_types(unicode))

        # Pair key to value 1
        key_value = words | 'PairWithOne' >> beam.Map(lambda x: (x, 1))

        # Count the occurrences of each word.
        counts = (key_value | 'GroupAndSum' >> beam.CombinePerKey(sum))

        # Format result set
        output = counts | beam.ParDo(FormatAsText()) #'Format' >> beam.Map(format_result)

        # Write the output using a "Write" transform.
        output | WriteToText(known_args.output)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()
